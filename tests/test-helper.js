import Application from 'ember-webslides/app';
import config from 'ember-webslides/config/environment';
import { setApplication } from '@ember/test-helpers';
import { start } from 'ember-qunit';

setApplication(Application.create(config.APP));

start();
