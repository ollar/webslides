import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | picker/pattern/hero-pattern', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<Picker::Pattern::HeroPattern />`);

    assert.equal(this.element.textContent.trim(), '');

    // Template block usage:
    await render(hbs`
      <Picker::Pattern::HeroPattern>
        template block text
      </Picker::Pattern::HeroPattern>
    `);

    assert.equal(this.element.textContent.trim(), 'template block text');
  });
});
