import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Serializer | giphy', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let store = this.owner.lookup('service:store');
    let serializer = store.serializerFor('giphy/gif');
    assert.ok(serializer);

    serializer = store.serializerFor('giphy/sticker');
    assert.ok(serializer);
  });

  test('it serializes records', function(assert) {
    let store = this.owner.lookup('service:store');

    let record = store.createRecord('giphy/gif', {});
    let serializedRecord = record.serialize();
    assert.ok(serializedRecord);

    record = store.createRecord('giphy/sticker', {});
    serializedRecord = record.serialize();
    assert.ok(serializedRecord);
  });
});
