import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Model | block/video content', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('block/video-content', {});
    assert.ok(model);
  });
});
