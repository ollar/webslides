import getRootUrl from 'ember-webslides/utils/get-root-url';
import { module, test } from 'qunit';

module('Unit | Utility | get-root-url', function() {

  // Replace this with your real tests.
  test('it works', function(assert) {
    let result = getRootUrl();
    assert.equal(result, '/');
  });
});
