import clickOutsideEl from 'ember-webslides/utils/click-outside-el';
import { module, test } from 'qunit';

module('Unit | Utility | click-outside-el', function() {

  // Replace this with your real tests.
  test('it works', function(assert) {
    let result = clickOutsideEl();
    assert.ok(result);
  });
});
