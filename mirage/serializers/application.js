import EmberDataSerializer from 'ember-cli-mirage/serializers/ember-data-serializer';
import { camelize, dasherize } from '@ember/string';

export default EmberDataSerializer.extend({
    primaryKey: '_id',

    normalize(payload) {
        let data = {};
        let type = this.type;

        data[type] = payload;

        return EmberDataSerializer.prototype.normalize.call(this, data);
    },
});
