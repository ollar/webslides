import config from 'ember-webslides/config/environment';
import User from './fixtures/user';

export default function() {

  // These comments are here to help you get started. Feel free to delete them.

  /*
    Config (with defaults).

    Note: these only affect routes defined *after* them!
  */

  this.urlPrefix = config.APP.apiHost;    // make this `http://localhost:8080`, for example, if your API is on a different server
  this.namespace = `/api/${config.APP.appName}`;    // make this `/api`, for example, if your API is namespaced
  this.timing = 400;      // delay for each request, automatically set to 0 during testing

  /*
    Shorthand cheatsheet:

    this.get('/posts');
    this.post('/posts');
    this.get('/posts/:id');
    this.put('/posts/:id'); // or this.patch
    this.del('/posts/:id');

    https://www.ember-cli-mirage.com/docs/route-handlers/shorthands
  */

  this.post(`${config.APP.authHost}/api/token`, () => {
    return {"refresh":"refreshToken","access":"accessToken"}
  });

  this.get(`${config.APP.apiHost}/me`, () => {
    return User[0];
  });

  this.resource('slide', { path: '/slides' });
  this.resource('project', { path: '/projects' });
  this.resource('user', { path: '/users' });
  this.resource('block', { path: '/blocks' });

  this.resource('block/text-content', { path: '/block/textContents' });

  this.passthrough('https://api.unsplash.com/*');
}
