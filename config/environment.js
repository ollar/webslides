"use strict";

require("dotenv").config();

const APP_NAME = "ember-webslides";
const { sentryDsn, giphyApiKey, unsplashAccess } = process.env;

module.exports = function (environment) {
    let ENV = {
        modulePrefix: APP_NAME,
        environment,
        rootURL: "/",
        locationType: "auto",
        EmberENV: {
            FEATURES: {
                // Here you can enable experimental features on an ember canary build
                // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
            },
            EXTEND_PROTOTYPES: {
                // Prevent Ember Data from overriding Date.parse.
                Date: false,
            },
        },

        APP: {
            // Here you can pass flags/options to your application instance
            // when it is created
            apiHost: 'http://34.242.87.146:8080',
            authHost: 'http://34.242.87.146:8090',
            // apiHost: 'http://localhost:8080',
            // authHost: 'http://localhost:8090',
            appName: APP_NAME,
            giphyApiKey,
            unsplashAccess,
        },
    };

    if (environment === "development") {
        // ENV.APP.LOG_RESOLVER = true;
        // ENV.APP.LOG_ACTIVE_GENERATION = true;
        // ENV.APP.LOG_TRANSITIONS = true;
        // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
        // ENV.APP.LOG_VIEW_LOOKUPS = true;

    }

    if (environment === "test") {
        // Testem prefers this...
        ENV.locationType = "none";

        // keep test console output quieter
        ENV.APP.LOG_ACTIVE_GENERATION = false;
        ENV.APP.LOG_VIEW_LOOKUPS = false;

        ENV.APP.rootElement = "#ember-testing";
        ENV.APP.autoboot = false;
        ENV.APP.appName = APP_NAME + 'test';
    }

    if (environment === "production") {
        // here you can enable a production-specific feature
        ENV.APP.sentryDsn = sentryDsn;
        // ENV.APP.apiHost = 'https://api.ollar.rocks';
        // ENV.APP.authHost = 'https://auth.ollar.rocks';

        ENV.APP.apiHost = 'http://45.135.165.155:8080';
        ENV.APP.authHost = 'http://45.135.165.155:8090';
    }

    return ENV;
};
