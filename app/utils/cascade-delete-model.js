export async function cascadeDeleteModel(model) {
    const modelsToDelete = [];
    const typesToIgnore = ['users', 'editors'];

    modelsToDelete.push(model);

    async function getModelsToDelete(_model) {
        if (!_model) return;

        for (const item of _model.constructor.relationshipsByName) {
            const type = item[0];
            const rel = item[1];

            switch (rel.kind) {
                case 'hasMany':
                    if (typesToIgnore.includes(type)) break;
                    for (const hm of _model.get(type).toArray()) {
                        if (hm) modelsToDelete.push(hm);
                        await getModelsToDelete(hm);
                    }
                    break;

                case 'belongsTo':
                    {
                        if (typesToIgnore.includes(type)) break;
                        const bt = await _model.get(type)
                        if (bt) modelsToDelete.push(bt);
                        await getModelsToDelete(bt);
                    }

            }
        }
    }

    await getModelsToDelete(model);
    return Promise.all(modelsToDelete.map(m => m.destroyRecord()));
}