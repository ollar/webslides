import { pluralize } from 'ember-inflector';

export async function dumpsModel(model, result = {}, _modelName) {
    const data = model.serialize({ includeId: true });
    const modelName = _modelName ?? model._internalModel.modelName;

    if (!result[pluralize(modelName)]) result[pluralize(modelName)] = {};

    result[pluralize(modelName)][data._id] = data;

    for (const item of model.constructor.relationshipsByName) {
        const type = item[0];
        const rel = item[1];

        if (rel.kind === 'hasMany') {
            for (const hm of model.get(type).toArray()) {
                await dumpsModel(hm, result);
            }
        }

        if (rel.kind === 'belongsTo') {
            await model.get(type).then(async (bt) => await dumpsModel(bt, result));
        }
    }

    return result;
}
