export async function cloneModel(template, store, _modelName) {
    const templateData = template.serialize();
    const modelName = _modelName ?? template._internalModel.modelName;
    const model = store.createRecord(modelName, {});

    for (const item of template.constructor.relationshipsByName) {
        const type = item[0];
        const rel = item[1];

        delete templateData[type];

        if (rel.kind === 'hasMany') {
            for (const hm of template.get(type).toArray()) {
                const hmmodel = await cloneModel(hm, store);
                model[type].pushObject(hmmodel);
            }
        }

        if (rel.kind === 'belongsTo') {
            const btmodel = await template.get(type).then(async (bt) => await cloneModel(bt, store));
            model[type] = btmodel;
        }
    }

    model.setProperties(templateData);
    return model.save();
}
