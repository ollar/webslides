import ColorThief from 'colorthief_dev123';

export default function getImgColor(img) {
    const colorThief = new ColorThief();
    let image;
    if (img instanceof HTMLImageElement) {
        image = img;
    } else {
        image = document.createElement('img');
        image.src = img;
        image.crossOrigin = 'anonymous';
    }
  return Promise.resolve()
    .then(() => {
        return new Promise((res) => {
            if (image.complete) {
              return res(colorThief.getColor(image));
            } else {
              image.addEventListener('load', function() {
                return res(colorThief.getColor(image));
              });
            }
        })
    })
}
