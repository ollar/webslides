import ENV from '../config/environment';

const { rootURL } = ENV;

export default function getRootUrl() {
    return rootURL;
}
