import { helper } from "@ember/component/helper";

export default helper(function eq(params /*, hash*/) {
    return params.reduce((a, b) => a === b);
});
