import { helper } from '@ember/component/helper';
import { htmlSafe } from '@ember/template';

export default helper(function safe(params/*, hash*/) {
  return params.map(p => htmlSafe(p)).join('');
});
