import Model, { attr } from '@ember-data/model';

export default class UnsplashPhotoModel extends Model {
    @attr created_at;
    @attr updated_at;
    @attr width;
    @attr height;
    @attr color;
    @attr description;
    @attr alt_description;
    @attr urls;
    @attr links;
    @attr categories;
    @attr user;
}