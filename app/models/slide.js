import Model, { attr, hasMany } from "@ember-data/model";


export default class SlideModel extends Model {
    @attr({ defaultValue: 0 }) index;
    @attr("number", { defaultValue: 4 }) gridSize;

    @attr({ defaultValue: "transparent" }) backgroundColor;
    @attr({ defaultValue: "center center" }) backgroundPosition;
    @attr({ defaultValue: "cover" }) backgroundSize;
    @attr({ defaultValue: 'repeat' }) backgroundRepeat;
    @attr({ defaultValue: 'center center' }) backgroundPosition;

    @attr({ defaultValue: null }) backgroundGradient;
    @attr({ defaultValue: null }) backgroundPattern;
    @attr({ defaultValue: null }) backgroundUnsplash;

    @attr({ defaultValue: 'fade' }) changeAnimation;

    @attr _uid;

    @hasMany blocks;

    async save() {
        if (this.isNew) return super.save(...arguments);

        const changedAttributes = this.changedAttributes();

        if (changedAttributes.backgroundUnsplash) {
            let newbi = changedAttributes.backgroundUnsplash[1];
            this.backgroundColor = newbi.color;
            this.backgroundGradient = null;
        }
        if (changedAttributes.backgroundGradient) {
            this.backgroundUnsplash = null;
        }

        // set backgroundSize to auto because pattenrs looks glitchy with backgroundSize = cover
        if (changedAttributes.backgroundPattern && !this.backgroundUnsplash) {
            this.backgroundSize = 'auto';
        }

        return super.save(...arguments);
    }
}
