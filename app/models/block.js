import Model, { attr, belongsTo } from "@ember-data/model";

export default class BlockModel extends Model {
    @attr("number", { defaultValue: 1 }) rowStart;
    @attr("number", { defaultValue: 2 }) rowEnd;
    @attr("number", { defaultValue: 1 }) columnStart;
    @attr("number", { defaultValue: 2 }) columnEnd;

    @attr("number", {defaultValue: 0}) padding;
    @attr("number", { defaultValue: 0 }) rotate;
    @attr('number', { defaultValue: 1 }) zIndex;

    @attr({ defaultValue: "transparent" }) backgroundColor;
    @attr({ defaultValue: "center center" }) backgroundPosition;
    @attr({ defaultValue: "cover" }) backgroundSize;
    @attr({ defaultValue: 'repeat' }) backgroundRepeat;

    @attr({ defaultValue: null }) backgroundGradient;
    @attr({ defaultValue: null }) backgroundPattern;
    @attr({ defaultValue: null }) backgroundUnsplash;

    @attr _uid;

    @belongsTo("block/content", { polymorphic: true }) content;

    save() {
        if (this.isNew) return super.save(...arguments);

        const changedAttributes = this.changedAttributes();

        if (changedAttributes.backgroundUnsplash) {
            this.backgroundGradient = null;
        }
        if (changedAttributes.backgroundGradient) {
            this.backgroundUnsplash = null;
        }

        // set backgroundSize to auto because pattenrs looks glitchy with backgroundSize = cover
        if (changedAttributes.backgroundPattern && !this.backgroundUnsplash) {
            this.backgroundSize = 'auto';
        }

        return super.save(...arguments);
    }
}
