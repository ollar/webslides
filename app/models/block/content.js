import Model, { attr } from "@ember-data/model";

export default class BlockContentModel extends Model {
    @attr contentType;
    @attr _uid;
}
