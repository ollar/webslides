import ContentModel from "./content";
import { attr } from "@ember-data/model";

export default class BlockImgContentModel extends ContentModel {
    @attr src;
    @attr giphy;
    @attr unsplash;
    @attr srcType;
    @attr({ defaultValue: 'cover' }) objectFit;
    @attr({ defaultValue: 'center center'}) objectPosition;

    save() {
        if (this.isNew) return super.save(...arguments);

        const changedAttributes = this.changedAttributes();

        if (changedAttributes.unsplash) {
            this.set('srcType', 'unsplash');
        } else if (changedAttributes.src) {
            this.set('srcType', 'src');
        } else if (changedAttributes.giphy) {
            this.set('srcType', 'giphy');
        }

        return super.save(...arguments);
    }
}
