import ContentModel from "./content";
import { attr } from "@ember-data/model";
import showdown from "showdown";
import { action } from "@ember/object";

export default class BlockTextContentModel extends ContentModel {
    @attr({ defaultValue: "" }) text;
    @attr({ defaultValue: "" }) htmlText;

    @attr({ defaultValue: 'inherit' }) fontFamily;
    @attr({ defaultValue: '1rem' }) fontSize;
    @attr({ defaultValue: 'textColor' }) color;
    @attr({ defaultValue: 'left' }) textAlign;
    @attr({ defaultValue: 'flex-start' }) alignItems;
    @attr({ defaultValue: 'horizontal-tb' }) writingMode;
    @attr({ defaultValue: 'mixed' }) textOrientation;

    @attr({ defaultValue: 0 }) letterSpacing;

    @action
    save() {
        if (this.isDeleted) return super.save(...arguments);
        if (this.isNew) return super.save(...arguments);

        const converter = new showdown.Converter({
            simpleLineBreaks: true,
            disableForced4SpacesIndentedSublists: true,
        }),
            text = this.text,
            html = converter.makeHtml(text);

        this.set("htmlText", html);

        return super.save(...arguments);
    }
}
