import ContentModel from "./content";
import { attr } from "@ember-data/model";

export default class BlockVideoContentModel extends ContentModel {
    @attr src;
    @attr coverr;

    @attr({ defaultValue: 'cover' }) objectFit;
    @attr({ defaultValue: 'center center'}) objectPosition;

    save() {
        if (this.isNew) return super.save(...arguments);

        const changedAttributes = this.changedAttributes();

        if (changedAttributes.coverr) {
            this.set('src', null);
        } else if (changedAttributes.src) {
            this.set('coverr', null);
        }

        return super.save(...arguments);
    }
}
