import Model, { attr, hasMany } from "@ember-data/model";
import { action } from "@ember/object";

export default class ProjectModel extends Model {
    @attr title;
    @attr created;
    @attr updated;
    @hasMany slides;
    @hasMany('user') editors;

    @attr _uid;

    @action
    save() {
        if (!this.isDeleted) this.updated = Date.now();
        return super.save(...arguments);
    }
}
