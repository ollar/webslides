import Model, { attr } from '@ember-data/model';

export default class GiphyGifModel extends Model {
    @attr url;
    @attr slug;
    @attr bitly_gif_url;
    @attr bitly_url;
    @attr embed_url;
    @attr username;
    @attr source;
    @attr title;
    @attr rating;
    @attr import_datetime;
    @attr trending_datetime;
    @attr images;
}
