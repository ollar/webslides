import Model, { attr } from '@ember-data/model';

export default class CoverrVideoModel extends Model {
    @attr createdAt;
    @attr updatedAt;
    @attr title;
    @attr poster;
    @attr thumbnail;
    @attr description;
    @attr isVertical;
    @attr tags;
    @attr playbackId;
    @attr aspectRatio;
    @attr duration;
    @attr d2VRatio;
    @attr baseFilename;
    @attr urls;
}
