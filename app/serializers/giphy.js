import RestSerializer from '@ember-data/serializer/rest';

export default class GiphySerializer extends RestSerializer {
    normalizeQueryResponse(store, primaryModelClass, payload, id, requestType) {
        const _payload = {
            [primaryModelClass.modelName]: payload.data,
        };
        return super.normalizeQueryResponse(store, primaryModelClass, _payload, id, requestType);
    }
}
