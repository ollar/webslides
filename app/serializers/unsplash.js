import RestSerializer from '@ember-data/serializer/rest';

export default class UnsplashSerializer extends RestSerializer {
    // normalizeFindAllResponse(store, primaryModelClass, payload, id, requestType) {
    //     const _payload = {
    //         [primaryModelClass.modelName]: payload,
    //     };
    //     return super.normalizeFindAllResponse(store, primaryModelClass, _payload, id, requestType);
    // }

    normalizeQueryResponse(store, primaryModelClass, payload, id, requestType) {
        const _payload = {
            [primaryModelClass.modelName]: payload.results || payload,
        };
        return super.normalizeQueryResponse(store, primaryModelClass, _payload, id, requestType);
    }

    normalizeQueryRecordResponse(store, primaryModelClass, payload, id, requestType) {
        const _payload = {
            [primaryModelClass.modelName]: payload,
        }
        return super.normalizeQueryRecordResponse(store, primaryModelClass, _payload, id, requestType);
    }
}
