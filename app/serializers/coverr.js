import RestSerializer from '@ember-data/serializer/rest';

export default class CoverrSerializer extends RestSerializer {
    normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
        const _payload = {
            [primaryModelClass.modelName]: payload.hits,
        };
        return super.normalizeArrayResponse(store, primaryModelClass, _payload, id, requestType);
    }
}
