import Component from '@glimmer/component';
import { action } from "@ember/object";
import { inject as service } from '@ember/service';
import { tracked } from "@glimmer/tracking";
import { htmlSafe } from '@ember/template';
import { task, taskGroup, timeout } from "ember-concurrency";
import Hammer from 'hammerjs';

export default class SlidesWrapperComponent extends Component {
    @service sizes;
    @service blockFocus;

    @tracked slidesWidth = 0;
    @tracked slideHeight = 0;

    focused = false;

    get activeSlideModel() {
        return this.args.activeSlideModel;
    }

    get cellWidth() {
        return Math.round(this.slidesWidth / this.activeSlideModel.gridSize);
    }

    get cellHeight() {
        return Math.round(this.slideHeight / this.activeSlideModel.gridSize);
    }

    get cellStyleVariables() {
        if (this.args.mode !== 'edit') return '';
        return htmlSafe(`--cell-width: ${this.cellWidth}px; --cell-height: ${this.cellHeight}px`);
    }

    get activeSlideChangeAvailable() {
        return (this.args.mode !== 'edit') || (this.args.mode === 'edit' && this.focused && !this.blockFocus.focusedBlockModel);
    }

    @taskGroup({
        restartable: true,
        maxConcurrency: 1,
    })
    focusTaskGroup;

    @task({ group: "focusTaskGroup" })
    *handleFocusTask() {
        yield timeout(10);
        this.focused = true;
    }

    @task({ group: "focusTaskGroup" })
    *handleBlurTask() {
        yield timeout(20);
        this.focused = false;
    }

    @action
    createResizeObserver(el) {
        this.hammertime = new Hammer(el);
        if (this.args.mode !== 'edit') return;
        this.slidesWidth = el.offsetWidth;
        this.slideHeight = this.sizes.vh;

        const { disconnect } = this.sizes.createResizeObserver(el, this.calculateSlideSize);

        this.disconnectResizeObserver = disconnect;
    }

    @action
    removeResizeObserver() {
        if (this.args.mode !== 'edit') return;
        this.disconnectResizeObserver && this.disconnectResizeObserver();
    }

    @action
    calculateSlideSize(entries) {
        const entry = entries[0];
        this.slidesWidth = entry.contentRect.width;
        this.slideHeight = this.sizes.vh;
    }

    @action
    handleFocus() {
        this.handleFocusTask.perform();
    }

    @action
    handleBlur() {
        this.handleBlurTask.perform();
    }

    @action
    handleKeyPress(e) {
        if (e.code === "Space" || e.code === "ArrowDown" || e.code === "ArrowRight") {
            this.goToNextSlide();
        } else if (e.code === "ArrowUp" || e.code === "ArrowLeft") {
            this.goToPrevSlide();
        }
    }

    @action
    addListeners() {
        document.addEventListener("keyup", this.handleKeyPress, {passive: true});
        this.hammertime.on('swipeleft swipeup', this.goToNextSlide);
        this.hammertime.on('swiperight swipedown', this.goToPrevSlide);
    }

    @action
    removeListeners() {
        document.removeEventListener("keyup", this.handleKeyPress, {passive: true});
        this.hammertime.off('swipeleft swipeup', this.goToNextSlide);
        this.hammertime.off('swiperight swipedown', this.goToPrevSlide);
        this.hammertime.destroy();
    }

    @action
    goToNextSlide() {
        if (!this.activeSlideChangeAvailable) return;
        return this.args.goToNextSlide();
    }

    @action
    goToPrevSlide() {
        if (!this.activeSlideChangeAvailable) return;
        return this.args.goToPrevSlide();
    }
}
