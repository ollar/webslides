import Component from "@glimmer/component";
import { action } from "@ember/object";
import { inject as service } from "@ember/service";
import absorb from 'ember-webslides/utils/absorb';

export default class ControlPanelComponent extends Component {
    @service store;
    @service blockFocus;
    @service projectManager;

    absorb = absorb;

    get projectModel() {
        return this.args.projectModel;
    }

    get focusedBlockModel() {
        return this.blockFocus.focusedBlockModel;
    }

    get slidesLength() {
        return this.projectModel.slides.length;
    }

    get canMoveBack() {
        return this.args.activeSlide > 1;
    }

    get canMoveForward() {
        return this.args.activeSlide < this.slidesLength;
    }

    get focusedBlockContentModel() {
        if (!this.focusedBlockModel) return null;
        const contentId = this.focusedBlockModel.get('content.id');
        const contentType = this.focusedBlockModel.get('content.contentType');
        return this.store.peekRecord(`block/${contentType}-content`, contentId);
    }

    @action
    addBlock(type) {
        this.projectManager
            .addBlockTask.perform(type);
    }

    @action
    removeBlock(block) {
        this.projectManager
            .removeBlockTask.perform(block);
    }
}
