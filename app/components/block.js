import Component from "@glimmer/component";
import { action } from "@ember/object";
import { task } from "ember-concurrency";
import { timeout } from "ember-concurrency";
import { inject as service } from "@ember/service";
import { htmlSafe } from '@ember/template';
import { tracked } from '@glimmer/tracking';
import Hammer from 'hammerjs';

const keyCodes = {
    esc: 'Escape',
    backspace: 'Backspace',
    delete: 'Delete'
};

export default class BlockComponent extends Component {
    @service blockFocus;
    @service projectManager;

    @tracked blockCssTransform = '';
    @tracked blockCssWidth = '';
    @tracked blockCssHeight = '';

    blockOffsetWidth = 0;
    blockOffsetHeight = 0;

    hammerMoveManager = null;
    hammerResizeManagers = [];

    get backgroundUnsplash() {
        const { data } = this.args;
        return data.backgroundUnsplash ?
            `url(${data.backgroundUnsplash?.urls?.regular})` : null;
    }

    get backgroundPattern() {
        const { data } = this.args;
        return data.backgroundPattern?.value;
    }

    get backgroundGradient() {
        const { data } = this.args;
        return data.backgroundGradient?.gradient;
    }

    get backgroundImage() {
        return [
                this.backgroundPattern,
                this.backgroundUnsplash,
                this.backgroundGradient
                ].filter(Boolean).join(',');
    }

    get backgroundSize() {
        const { data } = this.args;
        if (data.backgroundPattern && (data.backgroundGradient || data.backgroundUnsplash)) {
            return `auto, ${data.backgroundSize}`;
        }
        return data.backgroundSize;
    }

    get blockStyle() {
        let blockStyle = `--grid-column-start: ${this.args.data.columnStart};
                         --grid-column-end: ${this.args.data.columnEnd};
                         --grid-row-start: ${this.args.data.rowStart};
                         --grid-row-end: ${this.args.data.rowEnd};
                         --background-color: ${this.args.data.backgroundColor};
                         --background-image: ${this.backgroundImage};
                         --background-size: ${this.backgroundSize};
                         --background-position: ${this.args.data.backgroundPosition};
                         --padding: calc(var(--gap) * ${this.args.data.padding});
                         --z-index: ${this.args.data.zIndex};`;

        if (this.blockCssTransform) blockStyle += this.blockCssTransform;
        if (this.blockCssWidth) blockStyle += this.blockCssWidth;
        if (this.blockCssHeight) blockStyle += this.blockCssHeight;

        return htmlSafe(blockStyle);
    }

    get focused() {
        return this.blockFocus.focusedBlockModel === this.args.data;
    }

    @task({
        restartable: true,
        maxConcurrency: 1,
    })
    *updateContentValueTask(name, value) {
        yield timeout(200);
        const model = this.args.data;
        const contentModel = model.content;

        contentModel.set(name, value);
        yield contentModel.content.save();
    }

    @task
    *saveModelTask(model) {
        yield model.save();
    }

    // =========================================================================

    @action
    saveRef(el) {
        this.blockRef = el;
    }

    @action
    updateContentValue(e) {
        e.preventDefault();
        const { value, name } = e.target;
        if (name) this.updateContentValueTask.perform(name, value);
    }

    @action
    handleFocus() {
        if (this.args.mode !== 'edit') return;
        this.blockFocus.handleBlockFocus(this.args.data);
        this.bindHammerMove(this.blockRef);
        this.bindKeyListeners();
    }

    @action
    handleBlur() {
        if (this.args.mode !== 'edit') return;
        this.blockFocus.handleBlockBlur();
        if (this.hammerMoveManager) this.hammerMoveManager.destroy();
        this.unbindKeyListeners();
    }

    @action
    bindHammerResize(el) {
        const hammerManager = new Hammer.Manager(el);
        hammerManager.add(new Hammer.Pan());

        hammerManager.on('panstart', this.handlePanStart);
        hammerManager.on('panend', this.handlePanEnd);
        hammerManager.on('panmove', this.handleResizePanMove);
        hammerManager.on('pancancel', this.handlePanEnd);

        this.hammerResizeManagers.push(hammerManager);
    }

    @action
    unbindHammerResize() {
        this.hammerResizeManagers.forEach(m => m.destroy());
    }

    @action
    bindHammerMove(el) {
        const hammerManager = new Hammer.Manager(el);
        hammerManager.add(new Hammer.Pan());

        hammerManager.on('panstart', this.handlePanMoveStart);
        hammerManager.on('panend', this.handlePanEnd);
        hammerManager.on('panmove', this.handleMovePanMove);
        hammerManager.on('pancancel', this.handlePanEnd);

        this.hammerMoveManager = hammerManager;
    }

    @action
    handlePanMoveStart(event) {
        if (event.target.nodeName === 'TEXTAREA') return this.hammerMoveManager.stop();
    }

    @action
    handlePanStart() {
        this.blockOffsetWidth = this.blockRef.offsetWidth;
        this.blockOffsetHeight = this.blockRef.offsetHeight;

        if (this.hammerMoveManager) this.hammerMoveManager.stop();
    }

    @action
    handleResizePanMove(event) {
        const { target } = event;

        if (this.hammerMoveManager) this.hammerMoveManager.stop();

        const blockWidth = this.blockOffsetWidth;
        const blockHeight = this.blockOffsetHeight;

        let x = event.deltaX;
        let y = event.deltaY;

        if (target.classList.contains('drag-top-right')) {
            this.blockCssTransform = `transform: translate(0, ${blockHeight - this.blockRef.offsetHeight}px);`;
            this.blockCssWidth = `width: ${blockWidth + x}px;`;
            this.blockCssHeight = `height: ${blockHeight - y}px;`;
        } else if (target.classList.contains('drag-bottom-right')) {
            this.blockCssWidth = `width: ${blockWidth + x}px;`;
            this.blockCssHeight = `height: ${blockHeight + y}px;`;
        } else if (target.classList.contains('drag-bottom-left')) {
            this.blockCssTransform = `transform: translate(${blockWidth - this.blockRef.offsetWidth}px, 0);`;
            this.blockCssWidth = `width: ${blockWidth - x}px;`;
            this.blockCssHeight = `height: ${blockHeight + y}px;`;
        } else if (target.classList.contains('drag-top-left')) {
            this.blockCssTransform = `transform: translate(${blockWidth - this.blockRef.offsetWidth}px, ${blockHeight - this.blockRef.offsetHeight}px);`;
            this.blockCssWidth = `width: ${blockWidth - x}px;`;
            this.blockCssHeight = `height: ${blockHeight - y}px;`;
        }
    }

    @action
    handlePanEnd() {
        const model = this.args.data;
        const $controlPanel = document.querySelector('.control-panel');
        const controlPanelWidth = $controlPanel.offsetWidth;
        let { top, right, bottom, left } = this.blockRef.getBoundingClientRect();
        let { gridSize } = this.args;

        left-=controlPanelWidth;
        right-=controlPanelWidth;

        let columnStart = (Math.round(left/this.args.cellWidth));
        let columnEnd = (Math.round(right/this.args.cellWidth));
        let rowStart = (Math.round(top/this.args.cellHeight));
        let rowEnd = (Math.round(bottom/this.args.cellHeight));

        if (columnStart < 0) columnStart = 0;
        if (rowStart < 0) rowStart = 0;
        if (columnEnd < 1) columnEnd = 1;
        if (rowEnd < 1) rowEnd = 1;

        if (columnStart > gridSize - 1) columnStart = gridSize - 1;
        if (rowStart > gridSize - 1) rowStart = gridSize - 1;
        if (columnEnd > gridSize) columnEnd = gridSize;
        if (rowEnd > gridSize) rowEnd = gridSize;

        model.set('columnStart', columnStart + 1);
        model.set('columnEnd', columnEnd + 1);
        model.set('rowStart', rowStart + 1);
        model.set('rowEnd', rowEnd + 1);

        this.blockCssTransform = '';
        this.blockCssWidth = '';
        this.blockCssHeight = '';

        this.saveModelTask.perform(model);
    }

    @action
    handleMovePanMove(event) {
        let {deltaX, deltaY } = event;
        this.blockCssTransform = `transform: translate(${deltaX}px, ${deltaY}px);`;
    }

    @action
    bindKeyListeners() {
        window.addEventListener('keyup', this.handleKeyPress);
        window.addEventListener('keydown', this.handleBlockDeletePress);
    }

    @action
    unbindKeyListeners() {
        window.removeEventListener('keyup', this.handleKeyPress);
        window.removeEventListener('keydown', this.handleBlockDeletePress);
    }

    @action
    handleKeyPress(e) {
        const { code } = e;

        if (code === keyCodes.esc) {
            this.handleBlur();
        }
    }

    @action
    handleBlockDeletePress(e) {
        const { code, target } = e;
        const { data } = this.args;

        if (target.nodeName === 'TEXTAREA' || target.nodeName === 'INPUT') return;

        if (code === keyCodes.backspace || code === keyCodes.delete) {
            e.preventDefault();
            this.handleBlur();

            this.projectManager
                .removeBlockTask.perform(data);
        }
    }
}
