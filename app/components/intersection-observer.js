import Component from '@glimmer/component';
import { action } from '@ember/object';
import { schedule } from '@ember/runloop';


export default class IntersectionObserverComponent extends Component {
    rootMargin = "0px";
    threshold = 0.2;
    prevRatio = 0.5;

    wrapperComponent = 'intersection-observer/wrapper';
    triggerComponent = 'intersection-observer/trigger';

    @action
    didInsertWrapper(el) {
        let options = {
            root: el,
            rootMargin: this.args.rootMargin || this.rootMargin,
            threshold: this.args.threshold || this.threshold,
        }

        this.observer = new IntersectionObserver(this.intersectionCallback, options);
    }

    @action
    didInsertTrigger(el) {
        schedule('afterRender', () => {
            this.observer.observe(el);
        })

    }

    @action
    destroyIntersectionObserver() {
        this.observer.disconnect();
    }

    @action
    intersectionCallback(entries) {
        entries.forEach(entry => {
            if (entry.intersectionRatio > this.prevRatio) {
                if (this.args.onVisible) this.args.onVisible(entry.target);
            } else {
                if (this.args.onInvisible) this.args.onInvisible(entry.target);
            }

            this.prevRatio = entry.intersectionRatio;
        });
    }
}
