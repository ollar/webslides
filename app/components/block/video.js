import Component from '@glimmer/component';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';

export default class BlockVideoComponent extends Component {
    get model() {
        return this.args.data;
    }

    get coverr() {
        return this.model.get('coverr');
    }

    get isCoverr() {
        return !!this.coverr;
    }

    get thumbnail() {
        return this.coverr.thumbnail;
    }

    get m3U8() {
        return this.coverr.urls.m3U8;
    }

    get mp4() {
        return this.coverr.urls.mp4;
    }

    get style() {
        return htmlSafe(`--object-fit: ${this.model.get('objectFit')}; --object-position: ${this.model.get('objectPosition')}`);
    }

    @action
    reloadSource($video) {
        $video.pause();
        $video.load();
        $video.play();
    }
}
