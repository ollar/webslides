import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';

export default class BlockTextComponent extends Component {
    get style() {
        const { data } = this.args;

        return htmlSafe(`
            --font-family: ${data.get('fontFamily')};
            --text-align: ${data.get('textAlign')};
            --font-size: ${data.get('fontSize')};
            --color: ${data.get('color')};
            --letter-spacing: ${data.get('letterSpacing')}px;
            --justify-content: ${data.get('alignItems')};
            --writing-mode: ${data.get('writingMode')};
            --text-orientation: ${data.get('textOrientation')};
        `);
    }

    resize(e) {
        const el = e instanceof HTMLElement ? e : e.target;

        if (window.getComputedStyle(el).writingMode === 'horizontal-tb') {
            el.style.height = '1px';
            el.style.height = el.scrollHeight + 'px';
        } else {
            el.style.height = '';
        }

    }
}
