import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';

export default class BlockImgComponent extends Component {
    get model() {
        return this.args.data;
    }

    get imgSrc() {
        if (this.model.get('srcType') === 'unsplash') {
            return this.model.get('unsplash.urls.regular');
        }

        if (this.model.get('srcType') === 'giphy') {
            return this.model.get('giphy.images.original.url');
        }

        return this.model.get('src');
    }

    get altText() {
        if (this.model.get('srcType') === 'unsplash') {
            return this.model.get('unsplash.alt_description');
        }
        if (this.model.get('srcType') === 'giphy') {
            return this.model.get('giphy.title');
        }

        return '';
    }

    get style() {
        return htmlSafe(`--object-fit: ${this.model.get('objectFit')}; --object-position: ${this.model.get('objectPosition')}`);
    }
}
