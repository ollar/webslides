import Component from "@glimmer/component";
import { htmlSafe } from '@ember/template';

export default class SlideComponent extends Component {
    get backgroundUnsplash() {
        const { data } = this.args;
        return data.backgroundUnsplash ?
            `url(${data.backgroundUnsplash?.urls?.regular})` : null;
    }

    get backgroundPattern() {
        const { data } = this.args;
        return data.backgroundPattern?.value;
    }

    get backgroundGradient() {
        const { data } = this.args;
        return data.backgroundGradient?.gradient;
    }

    get backgroundImage() {
        return [
                this.backgroundPattern,
                this.backgroundUnsplash,
                this.backgroundGradient
                ].filter(Boolean).join(',');
    }

    get backgroundSize() {
        const { data } = this.args;
        if (data.backgroundPattern && (data.backgroundGradient || data.backgroundUnsplash)) {
            return `auto, ${data.backgroundSize}`;
        }
        return data.backgroundSize;
    }

    get slideStyle() {
        return htmlSafe(`--grid-size: ${this.args.data.gridSize}; --background-color: ${this.args.data.backgroundColor}; --background-image: ${this.backgroundImage}; --background-size: ${this.backgroundSize}; --background-position: ${this.args.data.get('backgroundPosition')}`);
    }
}

