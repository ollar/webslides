import Component from '@glimmer/component';

const verticalAlignVariants = ['flex-start', 'center', 'flex-end'];

export default class PickerVerticalAlignComponent extends Component {
    verticalAlignVariants = verticalAlignVariants;
}
