import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { task, timeout } from 'ember-concurrency';

export default class PickerUnsplashComponent extends Component {
    @service unsplash;
    @service store;
    @tracked search = '';

    get photosAreLoading() {
        return this.unsplash.unsplashApiCall.isRunning;
    }

    get photos() {
        return this.unsplash.results;
    }

    @action
    getList() {
        this.unsplash.list.perform();
    }

    @task({
        drop: true
    })
    *handleSelectTask({ name, value }) {
        const { contentEl } = this.args;
        const model = this.args.data;
        const unsplash = this.store.peekRecord('unsplash/photo', value);

        model.set(name, unsplash.serialize({ includeId: true }));

        if (contentEl) contentEl.style.opacity = 0.1;
        yield model.save();
        yield timeout(1000);
        if (contentEl) contentEl.style.opacity = '';
    }

    @action
    handleSelect(e) {
        e.preventDefault();
        e.stopPropagation();

        const { name, value } = e.target;

        this.handleSelectTask.perform({ name, value });
    }

    @action
    performSearch(e) {
        e.preventDefault();
        e.stopPropagation();
        const { target } = e;
        const { value } = target;

        if (!value) return this.unsplash.list.perform();

        this.unsplash.search.perform(value);
    }

    @action
    nextPage() {
        this.unsplash.fetchNextPage.perform();
    }
}
