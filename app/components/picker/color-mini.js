import Component from '@glimmer/component';
import { action } from "@ember/object";


const palette = ["#1abc9c", "#16a085", "#2ecc71", "#27ae60", "#3498db", "#2980b9", "#9b59b6", "#8e44ad", "#34495e", "#2c3e50", "#000000", "#f1c40f", "#f39c12", "#e67e22", "#d35400", "#e74c3c", "#c0392b", "#ecf0f1", "transparent"];

export default class PickerColorMiniComponent extends Component {
    palette = palette;

    @action
    onChange() {
        if (this.args.onChange) this.args.onChange(...arguments);
    }
}
