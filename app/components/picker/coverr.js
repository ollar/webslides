import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class PickerCoverrComponent extends Component {
    @service coverr;
    @service store;

    get videos() {
        return this.coverr.results;
    }

    get showLoader() {
        return this.coverr.coverrApiCall.isRunning;
    }

    @action
    getRandomVideos() {
        this.coverr.popular();
    }

    @action
    handleSelect(e) {
        e.preventDefault();
        e.stopPropagation();
        const { name, value } = e.target;
        const model = this.args.data;

        const coverr = this.store.peekRecord('coverr/video', value);
        model.set(name, coverr.serialize({ includeId: true }));
        model.save();
    }

    @action
    performSearch(e) {
        e.preventDefault();
        e.stopPropagation();
        const { target } = e;
        const { value } = target;

        if (!value) return this.getRandomVideos();

        this.coverr.search(value);
    }
}
