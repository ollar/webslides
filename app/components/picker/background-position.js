import Component from '@glimmer/component';

const verticalPoints = ['top', 'center', 'bottom'];
const horizontalPoints = ['left', 'center', 'right'];

export default class PickerBackgroundPositionComponent extends Component {
    get points() {
        return verticalPoints.map(vpoint => horizontalPoints.map(hpoint => `${hpoint} ${vpoint}`));
    }
}
