import Component from '@glimmer/component';

const variants = ['fade', 'vertical', 'horizontal'];

export default class PickerSlideChangeAnimationComponent extends Component {
    variants = variants;
}
