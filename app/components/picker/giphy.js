import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { task, timeout } from 'ember-concurrency';


export default class PickerGiphyComponent extends Component {
    @service giphy;
    @service store;
    @tracked search = '';
    @tracked activeGiphyType = '';

    get showLoader() {
        return this.giphy.giphyApiCall.isRunning;
    }

    get images() {
        return this.giphy.results;
    }

    @task({
        drop: true
    })
    *handleSelectTask({ name, value }) {
        const model = this.args.data;
        const { contentEl } = this.args;

        if (contentEl) contentEl.style.opacity = 0.1;
        const sticker = this.store.peekRecord(`giphy/${this.activeGiphyType}`, value);
        model.set(name, sticker.serialize({ includeId: true }));
        yield model.save();
        yield timeout(1000);
        if (contentEl) contentEl.style.opacity = '';
    }

    @action
    setGiphyType(type) {
        this.activeGiphyType = type;
    }

    @action
    getStickersList() {
        this.giphy.stickers.perform('popular');
    }

    @action
    getGifsList() {
        this.giphy.gifs.perform('popular');
    }


    @action
    handleSelect(e) {
        e.preventDefault();
        e.stopPropagation();
        const { name, value } = e.target;

        this.handleSelectTask.perform({ name, value });
    }

    @action
    performSearch(e) {
        e.preventDefault();
        e.stopPropagation();
        const { target } = e;
        const { value } = target;

        const searchFn = this.activeGiphyType === 'sticker' ? this.giphy.stickers : this.giphy.gifs;

        if (!value) return searchFn.perform('popular');

        return searchFn.perform(value);
    }

    @action
    loadMore(type) {
        this.giphy.loadMore.perform(type);
    }
}
