import Component from '@glimmer/component';

const fonts = [
    'inherit',
    'Arial',
    'Roboto',
    'Times New Roman',
    'Times',
    'Courier New',
    'Courier',
    'Verdana',
    'Georgia',
    'Palatino',
    'Garamond',
    'Bookman',
    'Comic Sans MS',
    'Candara',
    'Arial Black',
    'Impact',
];

export default class PickerFontComponent extends Component {
    fonts = fonts;
}
