import Component from '@glimmer/component';

const textAlignVariants = ['left', 'center', 'right'];

export default class PickerTextAlignComponent extends Component {
    textAlignVariants = textAlignVariants;
}
