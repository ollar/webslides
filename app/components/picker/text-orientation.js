import Component from '@glimmer/component';

const variants = ['mixed', 'upright'/* , 'sideways', 'sideways-right'*/];

export default class PickerTextOrientationComponent extends Component {
    variants = variants;
}
