import Component from '@glimmer/component';
import { action } from '@ember/object';
import { task, timeout } from "ember-concurrency";
import getRootUrl from 'ember-webslides/utils/get-root-url';

const variants = {
    'Aare': `url('${getRootUrl()}patterns/Aare.svg')`,
    'Clarence': `url('${getRootUrl()}patterns/Clarence.svg')`,
    'Doubs': `url('${getRootUrl()}patterns/Doubs.svg')`,
    'Hinterrhein': `url('${getRootUrl()}patterns/Hinterrhein.svg')`,
    'Inn': `url('${getRootUrl()}patterns/Inn.svg')`,
    'Kander': `url('${getRootUrl()}patterns/Kander.svg')`,
    'Linth': `url('${getRootUrl()}patterns/Linth.svg')`,
    'Mataura': `url('${getRootUrl()}patterns/Mataura.svg')`,
    'Mohaka': `url('${getRootUrl()}patterns/Mohaka.svg')`,
    'Ngaruroro': `url('${getRootUrl()}patterns/Ngaruroro.svg')`,
    'Oreti': `url('${getRootUrl()}patterns/Oreti.svg')`,
    'Rangitikei': `url('${getRootUrl()}patterns/Rangitikei.svg')`,
    'Reuss': `url('${getRootUrl()}patterns/Reuss.svg')`,
    'Rhône': `url('${getRootUrl()}patterns/Rhône.svg')`,
    'Taieri': `url('${getRootUrl()}patterns/Taieri.svg')`,
    'Thur': `url('${getRootUrl()}patterns/Thur.svg')`,
    'Vorderrhein': `url('${getRootUrl()}patterns/Vorderrhein.svg')`,
    'Waiau': `url('${getRootUrl()}patterns/Waiau.svg')`,
    'Waihou': `url('${getRootUrl()}patterns/Waihou.svg')`,
    'Waimakariri': `url('${getRootUrl()}patterns/Waimakariri.svg')`,
    'Wairau': `url('${getRootUrl()}patterns/Wairau.svg')`,
    'Whangaehu': `url('${getRootUrl()}patterns/Whangaehu.svg')`,
};

export default class PickerPatternPaaatternsComponent extends Component {
    variants = variants;

    @task({
        drop: true
    })
    *handleChangeTask({ key, value }) {
        const { data, name, contentEl } = this.args;
        data[name] = {
            key,
            value
        }

        if (contentEl) contentEl.style.opacity = 0.1;
        yield data.save();
        yield timeout(1000);
        if (contentEl) contentEl.style.opacity = '';
    }

    @action
    handleChange(key, value, e) {
        e.preventDefault();
        e.stopPropagation();

        this.handleChangeTask.perform({ key, value });
    }
}
