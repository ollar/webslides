import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import getRootUrl from 'ember-webslides/utils/get-root-url';
import { singularize } from 'ember-inflector';


function fetchLocal(url) {
  return new Promise(function(resolve, reject) {
    var xhr = new XMLHttpRequest
    xhr.onload = function() {
      resolve(new Response(xhr.responseText, {status: xhr.status}))
    }
    xhr.onerror = function() {
      reject(new TypeError('Local request failed'))
    }
    xhr.open('GET', url)
    xhr.send(null)
  })
}


export default class PickerSlidesComponent extends Component {
    @service store;
    @service me;

    @tracked templates = null;
    @tracked projects = null;

    @action
    initPicker() {
        fetchLocal(`${getRootUrl()}_templates.json`).then(res => res.json())
                .then((json) => {
                    json.forEach(item => {
                        Object.keys(item).forEach(entityKey => {
                            const _entityKey = entityKey === 'slides' ? 'templates' : entityKey;

                            Object.keys(item[entityKey]).forEach(key => {
                                this.store.push(
                                    this.store.normalize(
                                        singularize(_entityKey),
                                        Object.assign({}, { _id: key }, item[entityKey][key])
                                    )
                                );
                            });
                        });
                    });

                    this.templates = this.store.peekAll('template');
                });

        this.me.fetch()
            .then(me => {
                if (!me) return;
                me.get('projects').then(projects => (this.projects = projects));
            });
    }

    @action
    onSelect() {
        this.args.closePopup();
        return this.args.onSelect(...arguments);
    }
}
