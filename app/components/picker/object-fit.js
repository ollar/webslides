import Component from '@glimmer/component';

const variants = [
    'contain',
    'cover',
    'fill',
    'none'
];

export default class PickerObjectFitComponent extends Component {
    variants = variants;
}
