import Component from '@glimmer/component';

const variants = [
    'auto',
    'cover',
    'contain',
];

export default class PickerBackgroundSizeComponent extends Component {
    variants = variants;
}
