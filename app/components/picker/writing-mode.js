import Component from '@glimmer/component';

const variants = ['horizontal-tb', 'vertical-lr', 'vertical-rl'];

export default class PickerWritingModeComponent extends Component {
    variants = variants;
}
