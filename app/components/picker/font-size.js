import Component from '@glimmer/component';

const sizes = ['0.5rem', '0.8rem', '1rem', '1.2rem', '1.5rem', '2rem', '2.5rem', '3rem', '4rem', '5rem', '6rem', '7rem', '8rem'];

export default class PickerFontSizeComponent extends Component {
    sizes = sizes;
}
