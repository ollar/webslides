import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class CpBlockComponent extends Component {
    @tracked expanded = false;

    cpBlockTitle = 'cp-block/title';
    cpBlockContent = 'cp-block/content';

    get cpBlockExtendedContent() {
        return this.expanded ? 'cp-block/extended-content' : '';
    }

    @action
    toggleExpanded() {
        this.expanded = !this.expanded;
    }
}
