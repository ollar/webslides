import Route from '@ember/routing/route';

export default class IndexProjectShowRoute extends Route {
    queryParams = {
        activeSlide: {
            replace: true,
        }
    }
}
