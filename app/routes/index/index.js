import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';


export default class IndexIndexRoute extends Route {
    @service me;

    model() {
        return this.me.fetch()
            .then(me => {
                if (!me) return [];
                return me.get('projects');
            });

        // return this.store.query('project', {

        // });
    }
}
