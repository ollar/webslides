import Route from "@ember/routing/route";
import { getOwner } from "@ember/application";
import { inject as service } from "@ember/service";
import { action } from "@ember/object";

import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";

export default class ApplicationRoute extends Route {
    @service intl;
    @service radio;

    get _sentryDsn() {
        return getOwner(this).application.sentryDsn;
    }

    get _appName() {
        return getOwner(this).application.name;
    }

    get _appVersion() {
        return getOwner(this).application.version;
    }

    beforeModel() {
        this.intl.setLocale("en-us");

        if (this._sentryDsn) {
            Sentry.init({
                dsn: this._sentryDsn,
                integrations: [new Integrations.BrowserTracing()],
                release: `${this._appName}@${this._appVersion}`,
                tracesSampleRate: 1.0,
            });
        }
    }

    @action
    error(error) {
        this.radio.send({
            type: 'error',
            text: error.message,
        });
    }
}
