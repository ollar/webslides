import Controller from "@ember/controller";
import { action } from "@ember/object";
import { inject as service } from '@ember/service';

export default class IndexIndexController extends Controller {
    @service intl;
    @service session;
    @service me;
    @service projectManager;

    get isAuthenticated() {
        return this.session.isAuthenticated;
    }

    @action
    createProject() {
        this.projectManager.createProjectAndTransitToRoute.perform(project =>
            this.transitionToRoute("index.project.update", project)
        );
    }

    @action
    deleteProject(project) {
        if (window.confirm(this.intl.t('global.sure-confirm'))) {
            this.projectManager.deleteProjectTask.perform(project);
        }
    }
}
