import Controller from "@ember/controller";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { inject as service } from '@ember/service';

export default class IndexProjectUpdateController extends Controller {
    queryParams = [
        {
            activeSlide: "slide",
        },
    ];

    @tracked activeSlide = 1;
    @service projectManager;
    @service radio;
    @service intl;
    @service devMode;

    get isDevMode() {
        return this.devMode.isDevMode;
    }

    get activeSlideModel() {
        return this.model.slides.objectAt(this.activeSlide - 1);
    }

    @action
    updateModelValue(model, e) {
        return this.projectManager
            .updateModelValueTask.perform(model, e);
    }

    @action
    goToNextSlide() {
        if (this.activeSlide >= this.model.slides.length) return;
        this.activeSlide += 1;
    }

    @action
    goToPrevSlide() {
        if (this.activeSlide <= 1) return this.activeSlide = 1;
        this.activeSlide -= 1;
    }

    @action
    addSlide(slide) {
        this.projectManager
            .addSlideTask.perform(slide, this.goToNextSlide);
    }

    @action
    saveSlideAsTemplate(e) {
        e.preventDefault();

        this.projectManager
            .saveSlideAsTemplateTask.perform(this.activeSlideModel, () => {
                this.radio.send({
                    type: 'succes',
                    text: this.intl.t('project.update.slide-saved'),
                })
            });
    }

    @action
    removeTemplate(template, e) {
        e.preventDefault();

        this.projectManager.removeTemplateTask.perform(template, () => {
            this.radio.send({
                type: 'succes',
                text: this.intl.t('project.update.template-removed'),
            })
        });
    }

    @action
    removeSlide() {
        this.projectManager
            .removeSlideTask.perform(this.goToPrevSlide);
    }
}
