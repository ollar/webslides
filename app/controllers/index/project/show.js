import Controller from '@ember/controller';
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { inject as service } from '@ember/service';
import { toLeft, toRight, toUp, toDown } from 'ember-animated/transitions/move-over';
import fade from 'ember-animated/transitions/fade';

const BACK = 'BACK';
const FORWARD = 'FORWARD';

export default class IndexProjectShowController extends Controller {
    queryParams = [
        {
            activeSlide: "slide",
        },
    ];

    @service fullscreen;
    @tracked activeSlide = 1;

    get activeSlideModel() {
        return this.model.slides.objectAt(this.activeSlide - 1);
    }

    get nextSlideModel() {
        return this.model.slides.objectAt(this.activeSlide);
    }

    @action
    rules({firstTime, oldItems, newItems }) {
        if (firstTime || !oldItems[0] || !newItems[0] || oldItems[0].index === newItems[0].index) return fade;

        const oldIndex = oldItems[0].index;
        const newIndex = newItems[0].index;
        const oldModel = oldItems[0].model;
        const newModel = newItems[0].model;
        const direction = oldIndex > newIndex ? BACK : FORWARD;

        const changeAnimation = direction === BACK ? oldModel.changeAnimation : newModel.changeAnimation;

        switch (changeAnimation) {
            case 'vertical':
                return direction === BACK ? toDown : toUp;

            case 'horizontal':
                return direction === BACK ? toRight : toLeft;

            default:
                return fade;
        }
    }

    @action
    goToNextSlide() {
        if (this.activeSlide >= this.model.slides.length) return;
        this.activeSlide += 1;
    }

    @action
    goToPrevSlide() {
        if (this.activeSlide <= 1) return;
        this.activeSlide -= 1;
    }
}
