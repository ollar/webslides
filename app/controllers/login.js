import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { htmlSafe } from '@ember/template';
import getRootUrl from 'ember-webslides/utils/get-root-url';

export default class LoginController extends Controller {
    @service session;
    @service unsplash;
    @service radio;

    get backgrounds() {
        return this.unsplash.results.map(unsplashModel => unsplashModel.get('urls.regular'));
    }

    get styles() {
        if (!this.backgrounds.length) return '';
        return htmlSafe(`background-image: url('${getRootUrl()}patterns/Hinterrhein.svg'), url('${this.backgrounds.firstObject}')`);
    }

    @action
    updateModelValue(e) {
        if (e && e.preventDefault) e.preventDefault();
        const { value, name } = e.target;

        if (name) this.model.set(name, value);
    }

    @action
    submit(e) {
        if (e && e.preventDefault) e.preventDefault();

        if (this.model.validate()) {
            const data = this.model.serialize();
            this.session.authenticate('authenticator:application', data)
                .then(() => this.transitionToRoute('index'))
                .catch(e => {
                    this.radio.send({
                        type: 'error',
                        text: e.detail
                    });
                })
        }
    }

    @action
    fetchBackgrounds() {
        this.unsplash.random.perform();
    }
}
