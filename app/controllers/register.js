import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { htmlSafe } from '@ember/template';
import getRootUrl from 'ember-webslides/utils/get-root-url';

export default class RegisterController extends Controller {
    @service session;
    @service unsplash;
    @service radio;

    get backgrounds() {
        return this.unsplash.results.map(unsplashModel => unsplashModel.get('urls.regular'));
    }

    get styles() {
        if (!this.backgrounds.length) return '';
        return htmlSafe(`background-image: url('${getRootUrl()}patterns/Taieri.svg'), url('${this.backgrounds.firstObject}')`);
    }

    @action
    updateModelValue(e) {
        if (e && e.preventDefault) e.preventDefault();
        const { value, name } = e.target;

        if (name) this.model.set(name, value);
    }

    @action
    submit(e) {
        if (e && e.preventDefault) e.preventDefault();

        const data = this.model.serialize();

        if (this.model.validate()) {
            return this.session.register(data)
                .then(() => this.session.authenticate('authenticator:application', data))
                .then(() => {
                    const user = this.store.createRecord('user', {
                        username: data.username,
                        email: data.email,
                    });

                    return user.save();
                })
                .then(() => this.transitionToRoute('index'))
                .catch(error => {
                    Object.keys(error).forEach(key =>
                        this.radio.send({
                            type: 'error',
                            text: error[key]
                        })
                    );
                });
        }
    }

    @action
    fetchBackgrounds() {
        this.unsplash.random.perform();
    }
}
