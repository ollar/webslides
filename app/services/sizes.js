import Service from '@ember/service';
import { task, timeout } from "ember-concurrency";

export default class SizesService extends Service {
    get vh() {
        return Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
    }

    get vw() {
        return Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
    }

    @task({
        restartable: true,
        maxConcurrency: 1,
    })
    *calculateElementSizes(entries, handleResize) {
        yield timeout(30);
        handleResize(entries);
    }

    createResizeObserver(el, handleResize) {
        const resizeObserver = new ResizeObserver(entries => {
            this.calculateElementSizes.perform(entries, handleResize);
        });

        resizeObserver.observe(el);

        return {
            resizeObserver,
            disconnect: () => resizeObserver.disconnect()
        };
    }
}
