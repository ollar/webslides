import Service from "@ember/service";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";
import { inject as service } from '@ember/service';
import { task, taskGroup, timeout } from "ember-concurrency";

export default class BlockFocusService extends Service {
    @tracked focusedBlockModel = null;

    @service projectManager;

    @taskGroup({
        restartable: true,
        maxConcurrency: 1,
    })
    blockFocusTaskGroup;

    @task({ group: "blockFocusTaskGroup" })
    *handleBlockFocusTask(blockModel) {
        yield timeout(100);
        this.focusedBlockModel = blockModel;
        // if (focusCallback) focusCallback();
    }

    @task({ group: "blockFocusTaskGroup" })
    *handleBlockBlurTask() {
        yield timeout(200);
        this.focusedBlockModel = null;
        // if (blurCallback) blurCallback();
    }

    @action
    handleBlockFocus() {
        this.handleBlockFocusTask.perform(...arguments);
    }

    @action
    handleBlockBlur() {
        this.handleBlockBlurTask.perform(...arguments);
    }
}
