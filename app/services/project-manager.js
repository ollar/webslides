import Service, { inject as service } from '@ember/service';
import { task, timeout } from "ember-concurrency";
import { getOwner } from '@ember/application';
import { cloneModel } from 'ember-webslides/utils/clone-model';
import { dumpsModel } from 'ember-webslides/utils/dumps-model';
import { cascadeDeleteModel } from 'ember-webslides/utils/cascade-delete-model';

export default class ProjectManagerService extends Service {
    @service store;
    @service blockFocus;
    @service intl;
    @service me;

    get controller() {
        return getOwner(this).lookup('controller:index.project.update');
    }

    get projectModel() {
        return this.controller.model;
    }

    get activeSlide() {
        return this.controller.activeSlide;
    }

    get activeSlideModel() {
        return this.controller.activeSlideModel;
    }

    @task({
        drop: true,
    })
    *createProjectAndTransitToRoute(callback) {
        const project = this.store.createRecord("project", {
            created: Date.now(),
            title: this.intl.t('global.new-project'),
        });
        const slide = this.store.createRecord("slide");
        const meModel = this.me.model;
        project.slides.pushObject(slide);
        project.editors.pushObject(meModel);

        meModel.projects.pushObject(project);

        yield slide.save();
        yield project.save();
        yield meModel.save();

        return callback(project);
    }

    @task({
        drop: true,
    })
    *deleteProjectTask(project) {
        const meModel = this.me.model;

        meModel.projects.removeObject(project);

        yield cascadeDeleteModel(project);
        yield meModel.save();
    }

    @task({
        restartable: true,
        maxConcurrency: 1,
    })
    *updateModelValueTask(model, e) {
        if (e && e.preventDefault) e.preventDefault();
        const { value, name } = e.target;

        if (name) model.set(name, value);

        yield timeout(1000);
        yield model.save();
    }

    @task({
        drop: true,
    })
    *addSlideTask(template = null, callback) {
        let slide;
        if (template) {
            slide = yield cloneModel(template, this.store, 'slide');
        } else {
            slide = this.store.createRecord('slide');
            yield slide.save();
        }

        this.projectModel.slides.insertAt(this.activeSlide, slide);

        yield this.projectModel.save();

        if (callback) callback();
    }

    @task({
        drop: true
    })
    *removeSlideTask(callback) {
        if (window.confirm(this.intl.t('global.sure-confirm'))) {

            const slide = this.activeSlideModel;

            if (this.projectModel.slides.length === 1) {
                let _slide = this.store.createRecord('slide');
                yield _slide.save();
                this.projectModel.slides.pushObject(_slide);
            }

            this.projectModel.slides.removeObject(slide);

            if (callback) callback();

            yield cascadeDeleteModel(slide);
            yield this.projectModel.save();
        }
    }

    @task({
        drop: true
    })
    *saveSlideAsTemplateTask(template, callback) {
        const _template = yield dumpsModel(template);
        const blob = new Blob([JSON.stringify(_template)], {type: 'application/json'});
        const url = window.URL.createObjectURL(blob);
        const $a = document.createElement('a');

        $a.href = url;
        $a.download = 'dump.json';

        document.body.appendChild($a);

        if (typeof MouseEvent === 'function') {
            $a.dispatchEvent(
                new MouseEvent('click', {
                    bubbles: true,
                    cancelable: true,
                    view: window,
                })
            );
        } else {
            const event = document.createEvent('Event');
            event.initEvent('click', true, true);
            $a.dispatchEvent(event);
        }

        document.body.removeChild($a);

        if (callback) callback();
    }

    @task({
        drop: true
    })
    *removeTemplateTask(template, callback) {
        if (window.confirm(this.intl.t('global.sure-confirm'))) {
            yield cascadeDeleteModel(template);

            if (callback) callback();
        }
    }

    @task({
        restartable: true,
        maxConcurrency: 1,
    })
    *activeSlideModelSaveTask() {
        yield timeout(1000);
        yield this.activeSlideModel.save();
    }

    @task({
        drop: true,
    })
    *addBlockTask(type) {
        const slide = this.activeSlideModel;

        const content = this.store.createRecord(`block/${type}-content`, {
            contentType: type,
        });

        const block = this.store.createRecord("block");

        block.content = content;

        slide.blocks.pushObject(block);


        // TODO: check why is this not working
        // yield Promise.all([
        //     content.save(),
        //     block.save(),
        //     slide.save(),
        // ]);

        yield content.save();
        yield block.save();

        this.blockFocus.handleBlockFocus(block);
        this.activeSlideModelSaveTask.perform();

        yield this.projectModel.save();
    }

    @task({
        drop: true,
        maxConcurrency: 1,
    })
    *removeBlockTask(block) {
        const slide = this.activeSlideModel;
        slide.blocks.removeObject(block);

        this.blockFocus.handleBlockBlur();
        this.activeSlideModelSaveTask.perform();

        yield cascadeDeleteModel(block);
        yield this.projectModel.save();
    }
}
