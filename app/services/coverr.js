import Service, { inject as service } from '@ember/service';
import { task, taskGroup, timeout } from "ember-concurrency";
import { tracked } from '@glimmer/tracking';

export default class CoverrService extends Service {
    @service store;
    @tracked results;

    page=0;
    page_size=12;
    sort_by='popularity';

    @taskGroup({
        restartable: true,
        maxConcurrency: 1,
    }) coverrApiCall;

    constructor() {
        super(...arguments);
        this.resetProperties();
    }

    get requestOptions() {
        return {
            camel_case: true,
            filters: 'is_vertical:false',
            urls: true,
            page: this.page,
            page_size: this.page_size,
            sort_by: this.sort_by,
        };
    }

    @task({ group: 'coverrApiCall' })
    *searchTask(query) {
        yield timeout(1000);
        this.results = yield this.store.query('coverr/video', {
            query,
            ...this.requestOptions
        });

        return this.results;
    }

    @task({ group: 'coverrApiCall' })
    *popularTask() {
        yield timeout(1000);
        this.results = yield this.store.query('coverr/video', this.requestOptions);

        return this.results;
    }

    // =============================================================================================

    popular() {
        return this.popularTask.perform();
    }

    search(query = '') {
        return this.searchTask.perform(query);
    }

    resetProperties() {
        this.page=0;
        this.page_size=12;
        this.sort_by='popularity';
    }
}
