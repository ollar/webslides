import Service, { inject as service } from '@ember/service';
import { task, taskGroup, timeout } from "ember-concurrency";
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class GiphyService extends Service {
    @service store;

    _limit = 25;
    _offset = 0;
    _rating = 'G';
    _lang = 'en';

    cache = {};

    @tracked results = [];

    @taskGroup({
        restartable: true,
        maxConcurrency: 1,
    }) giphyApiCall;

    constructor() {
        super(...arguments);
        this.resetProperties();
    }

    get requestOptions() {
        return {
            limit: this._limit,
            offset: this._offset,
            rating: this._rating,
            lang: this._lang
        }
    }

    @task({ group: 'giphyApiCall' })
    *stickersTask(q) {
        yield timeout(1000);
        let results = yield this.store.query('giphy/sticker', {
            q,
            ...this.requestOptions
        });
        return results.toArray();
    }

    @task({ group: 'giphyApiCall' })
    *gifsTask(q) {
        yield timeout(1000);
        let results = yield this.store.query('giphy/gif', {
            q,
            ...this.requestOptions
        });
        return results.toArray();
    }

    @task
    *stickers(q) {
        this.resetProperties();
        this.cache = { q };
        this.results = yield this.stickersTask.perform(q);
        return this.results;
    }

    @task
    *gifs(q) {
        this.resetProperties();
        this.cache = { q };
        this.results = yield this.gifsTask.perform(q);
        return this.results;
    }

    @task
    *loadMore(type) {
        const task = type + 'Task';
        this._offset = this._offset + this._limit;
        const { q } = this.cache;
        const results = yield this[task].perform(q);
        this.results = [...this.results, ...results];
        return this.results;
    }

    @action
    resetProperties() {
        this._limit = 25;
        this._offset = 0;
        this._rating = 'G';
        this._lang = 'en';
    }
}
