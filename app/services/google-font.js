import Service from '@ember/service';
import getRootUrl from '../utils/get-root-url';

export default class GoogleFontService extends Service {
    getFontsList() {
        fetch(getRootUrl()+'gfonts/fonts-list.js').then(res => res.json()).then(console.log)
    }
}
