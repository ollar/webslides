import Service from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class FullscreenService extends Service {
    @tracked enabled = false

    constructor() {
        super(...arguments);
        document.addEventListener('fullscreenchange', () => {
            this.enabled = Boolean(document.fullscreenElement);
        });
    }

    @action
    enable() {
        document.documentElement.requestFullscreen();
    }

    @action
    disable() {
        document.exitFullscreen();
    }

    @action
    toggle() {
        return this.enabled ?
           this.disable() :
           this.enable();
    }
}
