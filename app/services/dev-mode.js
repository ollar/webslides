import Service from '@ember/service';

export default class DevModeService extends Service {
    get devMode() {
        const devMode = localStorage.getItem('devmode');

        try {
            return JSON.parse(devMode);
        } catch {
            return false;
        }
    }

    get isDevMode() {
        return this.devMode === true;
    }
}
