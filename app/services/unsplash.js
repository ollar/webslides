import Service, { inject as service } from '@ember/service';
import { task, taskGroup, timeout } from "ember-concurrency";
import { action } from "@ember/object";
import { tracked } from '@glimmer/tracking';

export default class UnsplashService extends Service {
    @service store;

    page = 1;
    per_page = 20;
    cache = {};

    @tracked results = [];

    @taskGroup({
        restartable: true,
        maxConcurrency: 1,
    }) unsplashApiCall

    @task({ group: 'unsplashApiCall' })
    *listTask(query, options) {
        yield timeout(300);
        let results = yield this.store.query('unsplash/photo', { query, page: this.page, per_page: this.per_page, ...options });
        return results.toArray();
    }

    @task({ group: 'unsplashApiCall' })
    *searchTask(query, options) {
        yield timeout(300);
        let results = yield this.store.query('unsplash/photo', { query, page: this.page, per_page: this.per_page, ...options });
        return results.toArray();
    }

    @task({ group: 'unsplashApiCall' })
    *randomTask(query, options) {
        yield timeout(300);
        let results = [yield this.store.queryRecord('unsplash/photo', { page: this.page, per_page: this.per_page, random: true, ...options})];
        return results;
    }

    @task
    *list(options = {}) {
        this.cache = {
            task: 'listTask',
            options,
        };

        this.page = 1;

        this.results = yield this.listTask.perform('', options);
        return this.results;
    }

    @task
    *search(query='') {
        this.cache = {
            task: 'searchTask',
            query,
        };

        this.page = 1;

        this.results = yield this.searchTask.perform(query);
        return this.results;
    }

    @task
    *random(options = {}) {
        this.cache = {
            task: 'randomTask',
            options,
        };

        this.page = 1;

        this.results = yield this.randomTask.perform('', options);
        return this.results;
    }

    @task
    *fetchNextPage() {
        const { task, query, options } = this.cache;
        this.page = this.page + 1;

        if (task) {
            let results = yield this[task].perform(query, options);
            this.results = [...this.results, ...results];
        }
    }
}
