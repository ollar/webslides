import EmberRouter from "@ember/routing/router";
import config from 'ember-webslides/config/environment';


export default class Router extends EmberRouter {
    location = config.locationType;
    rootURL = config.rootURL;
}

Router.map(function () {
  this.route("index", { path: "/" }, function () {
      this.route("index", { path: "/" });
      this.route("project", function () {
          this.route("show", { path: ":project_id" });
          this.route("update", { path: ":project_id/edit" });
      });
  });
  this.route('login');
  this.route('logout');
  this.route('register');
});
