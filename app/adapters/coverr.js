import RestAdapter from '@ember-data/adapter/rest';

export default class CoverrAdapter extends RestAdapter {
    host = 'https://coverr.co';
    namespace = 'api';
}
