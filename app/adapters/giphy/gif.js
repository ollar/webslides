import GiphyAdapter from '../giphy';

export default class GiphyGifAdapter extends GiphyAdapter {
    urlForQuery() {
        return `${this.urlPrefix()}/gifs/search?api_key=${this._giphyApiKey}`;
    }
}
