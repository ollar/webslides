import GiphyAdapter from '../giphy';

export default class GiphyStickerAdapter extends GiphyAdapter {
    urlForQuery() {
        return `${this.urlPrefix()}/stickers/search?api_key=${this._giphyApiKey}`;
    }
}
