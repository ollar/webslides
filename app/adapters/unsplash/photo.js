import UnsplashAdapter from '../unsplash';

export default class UnsplashPhotoAdapter extends UnsplashAdapter {
    urlForFindRecord(id) {
        return `${this.urlPrefix()}/photos/${id}`;
    }

    // urlForFindAll(modelName, snapshot) {
    //     let {
    //         adapterOptions
    //     } = snapshot;

    //     let query = Object.keys(adapterOptions).map(key => `${key}=${adapterOptions[key]}`).join('&');

    //     return `${this.urlPrefix()}/photos?${query}`;
    // }

    urlForQuery(query) {
        if (!query.query)
            return `${this.urlPrefix()}/photos`;
        return `${this.urlPrefix()}/search/photos`;
    }

    urlForQueryRecord(query) {
        if (query.random === true) {
            return `${this.urlPrefix()}/photos/random`;
        }

        return super.urlForQueryRecord(...arguments);
    }
}
