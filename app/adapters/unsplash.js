import RestAdapter from '@ember-data/adapter/rest';
import { getOwner } from '@ember/application';

export default class UnsplashAdapter extends RestAdapter {
    host = 'https://api.unsplash.com';

    get _unsplashAccess() {
        return getOwner(this).application.unsplashAccess;
    }

    get headers() {
        return {
            Authorization: `Client-ID ${this._unsplashAccess}`,
        };
    }
}
