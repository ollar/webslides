import CoverrAdapter from '../coverr';

export default class CoverrVideoAdapter extends CoverrAdapter {
    buildURL() {
        return `${this.urlPrefix()}/videos`;
    }

}
