import RestAdapter from '@ember-data/adapter/rest';
import { getOwner } from '@ember/application';

export default class GiphyAdapter extends RestAdapter {
    host = 'https://api.giphy.com';
    namespace = 'v1';

    get _giphyApiKey() {
        return getOwner(this).application.giphyApiKey;
    }
}
